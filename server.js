const express = require('express');
const logger = require('morgan-body');
const bodyParser = require('body-parser');

const app = express();

// extract request stream and allow interfacing with req.body 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//will be used when implementing forms for this app

//setup server routing to be used for site traversing
const school_routing = require('./app/routes/main-routing');
const student_routing = require('./app/routes/student-routing');
const teacher_routing = require('./app/routes/teacher-routing');
const course_routing = require('./app/routes/course-routing');

//use all routes in a given route folder
app.use('/', school_routing);
app.use('/', student_routing);
app.use('/', teacher_routing);
app.use('/', course_routing);

//test our database connection
const school_database = require('./db/connection');
school_database.testConnection();
school_database.logConnection();

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`\n APP listening on ${PORT}.... \n`);
});