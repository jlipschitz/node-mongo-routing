const mongoose = require('mongoose');
const config = require('../app/config/config.js');
const database = mongoose.connect(`mongodb://${config.db.school.host}/${config.db.school.collection}`).connection;

//plugin promise library to replace deprecated native mongoose promises
mongoose.Promise = require('bluebird');

module.exports = {
    db: database,
    testConnection: () => {
        database.on('error', console.error.bind(console, 'connection error:'));
    },
    logConnection: () => {
        database.once('open', () => {
            console.log(' Mongo connection successful at: Schools collection \n \n');
        });
    }
};