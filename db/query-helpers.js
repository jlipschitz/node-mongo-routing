"use strict";
module.exports = {
    //find all mongodb object in a collection and return it
    all: (collection) => {
        return collection.find({}, (err, data) => {
            if (err) return console.error(err);

            return data;
        });
    },
    //find all mongodb objects in a collection based on a query and return it
    allWithQuery: (collection, query) => {
        return collection.find({}, (err, data) => {
            if (err) return console.error(err);

            return data;
        });
    },
    //find one mongodb object in a collection based on a query and return it
    oneWithQuery: (collection, query) => {
        return collection.findOne(query, (err, data) => {
            if (err) return console.error(err);

            return data;
        });
    },
    //find referenced objects with mongo populate function
    populateWithQuery: (collection, query, popQuery) => {
        return collection
            .findOne(query)
            .populate(popQuery)
            .exec((err, result) => {
                if (err) return console.error(err);
                console.log(result);
                return result;
            });
    }
}