const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const courseSchema = Schema({
    name: String,
    capacity: Number,
    code: String,
    teacher: { type: Schema.Types.ObjectId, ref: 'teachers' },
    students: [{ type: Schema.Types.ObjectId, ref: 'students' }]
});

module.exports = mongoose.model('courses', courseSchema);