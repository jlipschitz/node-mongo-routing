const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const teacherSchema = Schema({
    name: String,
    age: Number,
    specialty: String,
});

module.exports = mongoose.model('teachers', teacherSchema);;