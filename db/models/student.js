const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const studentSchema = Schema({
    name: String,
    age: Number,
    grade: Number
});

module.exports = mongoose.model('students', studentSchema);;