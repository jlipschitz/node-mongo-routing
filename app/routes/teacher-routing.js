"use strict";
const express = require('express');
const app = express();
const router = express.Router();
const model_path = './../../db/models/';

//require our models and mongo helper functions
const Teacher = require(`${model_path}teacher`),
    gather = require(`./../../db/query-helpers`);

//find all teachers based on name
router.get('/find-teacher/name/:name', (req, res) => {
    let query = { name: req.params.name.toLowerCase() };

    Teacher.find(query, (err, doc) => {
        if (err) {
            console.error(err)
            res.end('We\'re sorry, unable to find your teacher based on name.');
        }
        console.log(doc);
        res.end(`Your teacher is ${doc} \n`);
    });
});

//find all teachers based on age
router.get('/find-teacher/age/:age', (req, res) => {
    let query = { age: req.params.age };

    Teacher.findOne(query, (err, doc) => {
        if (err) {
            console.error(err)
            res.end('We\'re sorry, unable to find your teacher based on age.');
        }
        console.log(doc);
        res.end(`Your teacher is ${doc} \n`);
    });
});

//find all teacher based on specialty
router.get('/find-teacher/specialty/:specialty', (req, res) => {
    let query = { specialty: req.params.specialty.toLowerCase() };

    Teacher.find(query, (err, doc) => {
        if (err) {
            console.error(err)
            res.end('We\'re sorry, unable to find your teacher based on specialty.');
        }
        console.log(doc);
        res.end(`Your teacher is ${doc} \n`);
    });
});

//save a new teacher
router.get('/add-teacher/:name/:age/:specialty', (req, res) => {
    let addTeacher = new Teacher({
        name: req.params.name.toLowerCase(),
        age: req.params.age,
        specialty: req.params.specialty.toLowerCase()
    });

    addTeacher.save((err, doc) => {
        if (err) {
            console.error('You had an error trying to save a teacher \n' + err);
            res.end('Unable to save teacher, sorry!')
        }
        console.log(`You've saved a new teacher \n ${doc} \n`);
        res.end(`Youv'e successfully added a new teacher: \n${doc} \n`)
    });
});

module.exports = router;