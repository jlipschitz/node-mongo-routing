"use strict";
const express = require('express');
const app = express();
const router = express.Router();
const model_path = './../../db/models/';

//require our models and mongo helper functions
const Student = require(`${model_path}student`),
    gather = require(`./../../db/query-helpers`);

//find one student based on name
router.get('/find-student/name/:name', (req, res) => {
    let query = { name: req.params.name.toLowerCase() };

    Student.findOne(query, (err, doc) => {
        if (err) {
            console.error(err)
            res.end('We\'re sorry, unable to find your teacher based on name.');
        }
        console.log(doc);
        res.end(`Your student is ${doc} \n`);
    });
});

//save a new student
router.get('/add-student/:name/:age', (req, res) => {
    let addStudent = new Student({
        name: req.params.name,
        age: req.params.age
    });

    addStudent.save((err, doc) => {
        if (err) {
            console.error('You had an error trying to save a student \n' + err);
            res.end('Unable to save student, sorry!')
        }
        console.log(`You've saved a new student \n ${doc} \n`);
        res.end(`Youv'e successfully added a new student: \n${doc}`)
    })
});

module.exports = router;