"use strict";
const express = require('express');
const app = express();
const router = express.Router();
const model_path = './../../db/models/';

//require our models and mongo helper functions
const Course = require(`${model_path}courses`),
    Student = require(`${model_path}student`),
    Teacher = require(`${model_path}teacher`),
    gather = require(`./../../db/query-helpers`);

//find one course based on name
router.get('/find-course/name/:name', (req, res) => {
    let query = { name: req.params.name.toLowerCase() };

    Course.findOne(query, (err, doc) => {
        if (err) {
            console.error(err)
            res.end('\n We\'re sorry, unable to find your Course based on name.\n');
        }
        console.log(doc);
        res.end(`Your Course is ${doc}\n`);
    });
});

//save an entirely new course
router.get('/add-course/:name/:capacity/:code', (req, res) => {
    let addCourse = new Course({
        name: req.params.name.toLowerCase(),
        capacity: req.params.age,
        code: req.params.code.toUpperCase()
    });

    addCourse.save((err, doc) => {
        if (err) {
            console.error(`\n You had an error trying to save a course \n ${err}\n`);
            res.end('\n Unable to save course, sorry!');
        }
        console.log(`You've saved a new course \n ${doc} \n`);
        res.end(`Youv'e successfully added a new course: \n${doc} \n`);
    })
});

//add students to a course
router.get('/add/student-courses/:coursecode/:name', (req, res) => {
    //query conditions to be inserted into mongo helper function
    let studentQuery = { name: req.params.name.toLowerCase() };
    let courseQuery = { code: req.params.coursecode.toUpperCase() };

    Promise.all([
            gather.oneWithQuery(Student, studentQuery),
            gather.oneWithQuery(Course, courseQuery)
        ])
        .then(results => {
            //store promise array objects returned from mongo query helper
            let student = results[0];
            let course = results[1];

            //return error if class is already full
            if (course.students.length >= course.capacity) {
                return res.end(`We're sorry this class is full! Capacity is ${JSON.stringify(course.capacity)} and ${course.students.length} are currently enrolled.`);
            }

            //add students to the course if there is space
            Course.update({ name: course.name, _id: course._id }, { $push: { students: student._id } }, (err, doc) => {
                if (err) {
                    console.error(err)
                    res.send(`Unable to add student to course due to error: \n${err}\n`);
                }
                res.end(`You've successfully added ${JSON.stringify(student.name)} to ${JSON.stringify(course.code)}`);
            });

        }, err => {
            console.error(err)
        }).catch(error => {
            //promise error handling
            console.error(error);
            res.end('unable to start adding student to course process, sorry!');
        });

});

router.get('/add/teacher-courses/:coursecode/:name', (req, res) => {
    //query conditions to be inserted into mongo helper function
    let teacherQuery = { name: req.params.name.toLowerCase() };
    let courseQuery = { code: req.params.coursecode.toUpperCase() };

    Promise.all([
            gather.oneWithQuery(Teacher, teacherQuery)
        ])
        .then(results => {
            //store promise array object returned from mongo query helper
            let teacher = results[0];

            //add teacher to course
            Course.update({ code: req.params.coursecode }, { $set: { teacher: teacher._id } }, (err, doc) => {
                if (err) {
                    console.error(err)
                    res.send(`Unable to add teacher to course due to error: \n${err}\n`);
                }
                res.send('Teacher was succesfully updated to the course!');
            });

        }, err => {
            console.error(err)
        }).catch(error => {
            //promise error handling
            console.error(error);
            res.end('unable to start adding teacher to course process, sorry!');
        });
});

module.exports = router;