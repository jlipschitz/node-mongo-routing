"use strict";
const express = require('express');
const app = express();
const router = express.Router();
const model_path = './../../db/models/';
const bodyParser = require('body-parser');

//require our models and mongo helper functions
const Teacher = require(`${model_path}teacher`),
    Students = require(`${model_path}student`),
    Course = require(`${model_path}courses`),
    gather = require(`./../../db/query-helpers`);

//gather all student, courses, teacher data and display 
router.get('/', (req, res) => {

    //call populateAll functions in sequence with promises
    Promise.all([gather.all(Teacher),
            gather.all(Students),
            gather.all(Course)
        ])
        .then(results => {
            //format results into object and respond back with data
            let data = { teachers: results[0], students: results[1], courses: results[2] };
            res.send(`${JSON.stringify(data)}`);

        }, err => {
            console.error(err)
        }).catch(error => {
            //promise error handling
            res.end('There was an error retrieving school data. Please try again.');
        });
});


module.exports = router;